import json
from datetime import datetime
from flask import Flask, render_template, request,redirect
from hashlib import sha512
app = Flask(__name__,
            static_url_path='/DATA', 
            static_folder='DATA')


@app.route('/')
def redirection():
    return redirect('blog')

@app.route('/blog/', methods = ['GET'])
def blog():
    return render_template('index.html')

def datajson():
    now = datetime.now()
    date_time = now.strftime("%Y%m%d%H%M%S")
   
 

    file='DATA/'+ date_time +'.json'
    with open (file, 'w') as outfile:
        d={
        "url": "http://127.0.0.1:5000/" + file,
        "contenu":request.form['post'],
        "date":date_time
        }
        json.dump(d,outfile)
    with open('./DATA/posts.json','r') as f:
        results = json.load(f)
        results.append("http://127.0.0.1:5000/" + file)
    with open('./DATA/posts.json','w') as f:
        json.dump(results, f, indent=4)
    return file

@app.route('/blog/', methods=['POST'])
def check_secret():
    with open('mdp.txt') as test_f:
        pwd_fichier = test_f.readline()
        a = request.form["secret"]
        a = a.encode()

    retour = sha512(a).hexdigest()

    if retour == pwd_fichier:
        return datajson()
    return redirect ("http://127.0.0.1:5000/", code=403)

if __name__ == "main":
    print("coucou")
    app.run(debug=True)